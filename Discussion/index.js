// DOM Selectors

// Query Selector
//const firstName = document.querySelector('#firstName');
//const lastName = document.querySelector('#lastName');

// Get Element by ID: for single-specific component/s.
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');

// Get Elements by Class Name: for multiple components at the same time.
const inputFields = document.getElementsByClassName('form-control');

// Get Elements by Tag Name: can be used when targeting elements of the same tags.
const heading3 = document.getElementsByTagName('h3')

console.log(firstName);
console.log(lastName);
console.log(inputFields);
console.log(heading3);